﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DemoCurs;
using Moq;
using Animal = AnimalHomework.Animal;

namespace Animals.Test
{
	[TestClass]
	public class AnimalControllerTest
	{
		[TestMethod]
		public void GetData_Test()
		{
			Mock<IAnimalRepository> repoMock = new Mock<IAnimalRepository>();

			Animal lion = new Animal
			{
				Id = 1,
                Name = "Lion",
                Description = "African lions are large, muscular, barrel-chested cats."
			};


			repoMock.Setup(x => x.Read(1)).Returns(p1);

			AnimalController controller = new AnimalController(repoMock.Object);
			Animal result = controller.Read(1);

			Assert.AreEqual(lion.Id,result.Id);
			Assert.AreEqual(lion.FirstName,result.Name);
			Assert.AreEqual(lion.LastName,result.Description);
		}

		[TestMethod]
		public void GetAllData_Test()
		{
			Mock<IAnimalRepository> mock = new Mock<IAnimalRepository>();

			Animal lion = new Animal
            {
                Id = 1,
                Name = "Lion",
                Description = "African lions are large, muscular, barrel-chested cats."
            };

            Animal wolf = new Animal
            {
                Id = 2,
                Name = "Wolf",
                Description = "The wolf is a canine native to the wilderness."
            };

      

			List<Animal> animal = new List<Animal>
			{
				lion,
				wolf
			};

			mock.Setup(x => x.ReadAll()).Returns(animal);

			AnimalController controller = new AnimalController(mock.Object);

			List<Animal> result = controller.ReadAll().ToList();

			Assert.AreEqual(2, result.Count);
		}
	}
}
