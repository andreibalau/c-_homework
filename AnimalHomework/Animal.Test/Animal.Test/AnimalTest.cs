﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using AnimalHomework;

namespace Animals.Test
{
    [TestClass]
	public class AnimalTest
	{
        Animal lion = new Animal
        {
            Id = 1,
            Name = "Lion",
            Description = "African lions are large, muscular, barrel-chested cats."
        };

        Animal wolf = new Animal
        {
            Id = 2,
            Name = "Wolf",
            Description = "The wolf is a canine native to the wilderness."
        };

        Animal dog = new Animal
        {
            Id = 3,
            Name = "Dog",
            Description = "Dogs are domesticated mammals, not natural wild animals."
        };

        [TestMethod]
		public void RetrieveCorrectData_Test()
		{
			AnimalRepository repo = new AnimalRepository();

			repo.Add(lion);
			repo.Add(wolf);

			Animal first = repo.Read(1);
			Animal second = repo.Read(2);

			Assert.AreEqual(lion.Name, first.FirstName);
			Assert.AreEqual(lion.Description, first.LastName);
			Assert.AreEqual(lion.Id, first.Id);
		}

		[TestMethod]
		public void RetrieveInvalidData_Test()
		{
			AnimalRepository repo = new AnimalRepository();

			repo.Add(lion);
			repo.Add(wolf);

			Animal first = repo.Read(1);
			Animal second = repo.Read(2);

			Assert.AreEqual(lion.Name, first.Name);
			Assert.AreEqual(lion.Description, first.Description);
			Assert.AreEqual(3, first.Id);
		}

        [TestMethod]
		public void EditCorrectData_Test()
		{
			AnimalRepository repo = new AnimalRepository();

			repo.Add(lion);
			repo.Add(wolf);

			Animal first = repo.Edit(1,"Pisica", "meow");
			//Animal second = repo.Edit(2,"Ceva","altceva");

			Assert.AreEqual(lion.Name, first.Name);
			Assert.AreEqual(lion.Description, first.Description);
			Assert.AreEqual(lion.Id, first.Id);
		}

		[TestMethod]
		public void EditInvalidData_Test()
		{
			AnimalRepository repo = new AnimalRepository();

			repo.Add(lion);
			repo.Add(wolf);
            Animal readFirst = repo.Read(1);

			Animal first = repo.Edit(1,"Pisica", "meow");
			Animal second = repo.Edit(2,"Ceva","altceva");

			Assert.AreEqual(readFirst.Name, first.Name);
            Assert.AreEqual(readFirst.Name, second.Name);
		}

        [TestMethod]
		public void DeleteCorrectData_Test()
		{
			AnimalRepository repo = new AnimalRepository();

			repo.Add(lion);
			repo.Add(wolf);

			repo.Delete(1);

            if(repo.ReadAll.Count==1){
                Assert.AreEqual(wolf.Id, wolf.Id);
            }else {
                    Assert.AreEqual(lion.Id, wolf.Id);
            }
			
		}

		[TestMethod]
		public void DeleteInvalidData_Test()
		{
			AnimalRepository repo = new AnimalRepository();

			repo.Add(lion);
			repo.Add(wolf);

			repo.Delete(1);

			Assert.AreEqual(lion.Id, wolf.Id);
		}
	}
}
