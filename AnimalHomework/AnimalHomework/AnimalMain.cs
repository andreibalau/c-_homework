using System;

namespace AnimalHomework
{
    public class AnimalMain
    {
        static void Main(string[] args)
        {
            Animal lion = new Animal
            {
                Id = 1,
                Name = "Lion",
                Description = "African lions are large, muscular, barrel-chested cats."
            };

            Animal wolf = new Animal
            {
                Id = 2,
                Name = "Wolf",
                Description = "The wolf is a canine native to the wilderness."
            };

            Animal dog = new Animal
            {
                Id = 3,
                Name = "Dog",
                Description = "Dogs are domesticated mammals, not natural wild animals."
            };

            AnimalRepository repo = new AnimalRepository();

            repo.Add(lion);
            repo.Add(wolf);
            repo.Add(dog);

            repo.Read(1);
            repo.Edit(3, "Cutu", "hamham");
            repo.Delete(2);
            var read = repo.ReadAll();

            foreach (var v in read)
            {
                Console.WriteLine(v.Name);
            }
        }
    }
}