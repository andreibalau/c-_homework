﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AnimalHomework
{
    public interface IAnimalRepository
    {
        void Add(Animal p);
        Animal Read(int id);

        void Edit(int id, string name, string description);
        void Delete(int id);
        List<Animal> ReadAll();
    }
}
