﻿using System;
using System.Collections.Generic;

namespace AnimalHomework
{
	public class AnimalController
	{
		readonly IAnimalRepository _animalRepository;

		public AnimalController(IAnimalRepository animalRepository)
		{
			_animalRepository = animalRepository;
		}


		public void Add(Animal p)
		{
			_animalRepository.Add(p);
		}

		public Animal Read(int id)
		{
			return _animalRepository.Read(id);
		}

        public List<Animal> ReadAll()
        {
            return _animalRepository.ReadAll();
        }
	}
}
