﻿using System;
using System.Collections.Generic;

namespace AnimalHomework
{

	// interact with database simulator
	public class AnimalRepository : IAnimalRepository
	{
		readonly List<Animal> _animal = new List<Animal>();

		public void Add(Animal a)
		{
			_animal.Add(a);
		}

		public Animal Read(int id)
		{
			foreach (var a in _animal)
			{
				if (a.Id == id)
				{
					return a;
				}
			}

			return null;
		}

        public void Edit(int id,string name=null, string description=null)
        {
            var animal = Read(id);
            if (animal!=null)
            {
                if (name!=null)
                {
                    animal.Name = name;
                }

                if (description != null)
                {
                    animal.Description = description;
                }
            }
        }

        public void Delete(int id)
        {
            var animal = Read(id);
            if (animal!=null)
            {
                _animal.Remove(animal);
            }
        }
        public List<Animal> ReadAll() => _animal;
	}
}
