﻿using System;

namespace IerarhieHomework
{
    class Persoana
    {
        public string Nume { get; set; }
        public string Prenume { get; set; }
        public string Adresa { get; set; }
    }
}
