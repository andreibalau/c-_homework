﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IerarhieHomework
{
    class MainIerarhie
    {

        public static void Main(string[] args)
        {
            List<Persoana> listAll = new List<Persoana>();
            Persoana p1 = new Persoana
            {
                Nume = "Balau",
                Prenume = "Andrei",
                Adresa = "Brasov str.Grivitei"
            };

            Persoana p2 = new Persoana
            {
                Nume = "Cabal",
                Prenume = "George",
                Adresa = "Poiana Marului"
            };

            Persoana p3 = new Persoana
            {
                Nume = "Banu",
                Prenume = "Georgiana",
                Adresa = "Poiana Marului"
            };

            Student s1 = new Student
            {
                Nume = "Neculoiu",
                Prenume = "Adrian",
                Adresa = "Zarnesti str. Celulozei",
                medie = 8.89,
                nrMatricol = "21"
                
            };

            Student s2 = new Student
            {
                Nume = "Uscoiu",
                Prenume = "Diana",
                Adresa = "Sinca Noua",
                medie = 9.45,
                nrMatricol = "234"
            };

            Student s3 = new Student
            {
                Nume = "Persoiu",
                Prenume = "Eduard",
                Adresa = "Poiana Marului str. Republicii",
                medie = 7.30,
                nrMatricol = "57"
            };
            listAll.Add(p1);
            listAll.Add(p2);
            listAll.Add(p3);
            listAll.Add(s1);
            listAll.Add(s2);
            listAll.Add(s3);

            Console.WriteLine("List size = "+listAll.Count);

            Console.WriteLine("\nFiltru dupa nume:\n");
            foreach (var p in listAll.OrderBy(o => o.Nume).ToList())
            {
                
                Console.WriteLine(p.Nume);
            }
            Console.WriteLine("\nFiltru dupa prenume:\n");
            foreach (var p in listAll.OrderBy(o => o.Prenume).ToList())
            {
                
                Console.WriteLine(p.Prenume);
            }

        }
    }
}
