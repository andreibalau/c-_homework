﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CalculatorHomework
{
    class MainProgram
    {
        public static void Main(string[] args)
        {
            Calculator obj = new Calculator();

            Console.Write($"Insert x: ");
            var x = Console.ReadLine();
            Console.Write($"Insert y: ");
            var y = Console.ReadLine();

            int a = Convert.ToInt32(x);
            int b = Convert.ToInt32(y);
            Console.Write($"Your input : {x} & {y}\n");

            Console.WriteLine(  "Select the operation on variables : \n" +
                                "Additon = 1\n" +
                                "Substraction = 2\n" +
                                "Multiplication = 3\n" +
                                "Division = 4\n" +
                                "Exit = 5\n");

            Console.Write("Chose your option : ");
            var choiceInput = Console.ReadLine();
            int choice = Convert.ToInt32(choiceInput);
            switch (choice)
            {
                case 1:
                    Console.WriteLine("Addintion chosed");
                    Console.WriteLine($"The result is : {obj.ADD(a, b)}");
                    break;
                case 2:
                    Console.WriteLine("Substraction chosed");
                    Console.WriteLine($"The result is : {obj.Sub(a, b)}");
                    break;
                case 3:
                    Console.WriteLine("Multiplication chosed");
                    Console.WriteLine($"The result is : {obj.Multi(a, b)}");
                    break;
                case 4:
                    Console.WriteLine("Division chosed");
                    Console.WriteLine($"The result is : {obj.Divi(a, b)}");
                    break;
                case 5:
                    Console.WriteLine("Exiting...");
                    break;
                default:
                    Console.WriteLine("Invalid option");
                    break;
            }
        }
    }
}

